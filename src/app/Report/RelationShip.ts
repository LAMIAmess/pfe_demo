export class RelationShip {
   tableName: string;
   columnName: string;
   foreignTableName: string;
   foreignColumnName: string;
}
