import {ParameterModel} from './ParameterModel';

export class ReportModel {
  parameters: ParameterModel[];
  fields: string[] ;
  table: string;
  constructor() {
  }
}
