import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import {HttpClient} from '@angular/common/http';
import {ReportModel} from '../../Report/ReportModel';
import {ParameterModel} from '../../Report/ParameterModel';
import * as moment from 'moment';
import {timestamp} from 'rxjs/operators';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
tables: any = [];
fields: any = [];
tab: string;
selectedFields: any;
typeField: any;

operateurs: [any][any] = ['test'];
parameterModel: ParameterModel ;
dataarray = [];
testbetween = [] ;
testdate = [];
  private dataarray1: any[];

  constructor(private dashboardService: DashboardService, private http: HttpClient) { }

  ngOnInit() {
    console.log('hello');
    this.getAllTables();
    this.getAllFields();

  }


  changeTable(tab: string) {
    console.log(tab);
    this.tab = tab;
    this.getAllFields();
    console.log('lamia');

  }
  clickField() {
    console.log(this.selectedFields);
  }

  async clickParameter(i: number) {
    console.log('parameter select ');

    this.getTypeOfField(this.tab, this.dataarray[i].parameter );
    await new Promise(resolve => setTimeout(() => resolve(), 500)).then(() => console.log('fired'));
    this.modifyForm(i);
    this.modifyOperator(i);

  }




  addCondition() {
    this.parameterModel = new ParameterModel();
    this.dataarray.push(this.parameterModel);
    this.testbetween.push(false);
    this.testdate.push(false);


  }

  removeConcdition(i: number) {
    this.dataarray.splice(i, 1);
  }

  register() {
    const report: ReportModel = new ReportModel();
    report.parameters = this.dataarray;
    report.fields = this.selectedFields;
    report.table = this.tab;
    console.log(report);



    this.http.post('http://localhost:1354/report/generate', report)
      .subscribe(data => {
        console.log('success');
      }, error => {
        console.log(error);
      });
  }

  getAllTables() {
    this.http.get('http://localhost:1354/requete/getAllTables')
      .subscribe(data => {
          this.tables = data;

        }
        , error => {
          console.log('error table');
        });
  }
  getAllFields() {
    this.http.get('http://localhost:1354/requete/getAllFields/' + this.tab)
      .subscribe(data => {
          this.fields = data;

        }
        , error => {
          console.log('error fields');
        });
  }
  getTypeOfField(tablename: string, fieldname: string) {
    this.http.get('http://localhost:1354/requete/getTypeOfField/' + tablename + '/' + fieldname, {responseType: 'text'})
      .subscribe(data => {
        console.log('success lamia');
        console.log(data);
        this.typeField = data;
      }, error => {
        console.log('erreur  lamia');
        console.log(error);
      });


  }


  private modifyOperator(i: number) {
    if (this.typeField === 'boolean') {

      this.operateurs[i]  = ['=', '!='];
    } else if (this.typeField === 'bigint' || this.typeField === 'numeric' || this.typeField === 'integer') {
      this.operateurs[i] = ['>', '<', '>=', '<=', '=', '!=', 'between'];
    } else if (this.typeField.includes('character') || this.typeField.includes('text')) {
      this.operateurs[i] = ['=', 'like'];
    } else if (this.typeField.includes('timestamp')) {
      this.operateurs[i] = ['>', '<', '>=', '<=' , '=', '!=', 'between'];
      }

  }

  clickOperator(i: number) {
    console.log('hello from home ');
    console.log(this.dataarray[i].operateur);
    if (this.dataarray[i].operateur === 'between') {
     this.testbetween[i] = true;
    } else {
      this.testbetween[i] = false;
    }
  }

  private modifyForm(i: number) {
    if (this.typeField.includes('timestamp')) {
      this.testdate[i] = true;
    } else {
      this.testdate[i] = false;
    }
  }

  changeDate(i: number, j: number) {
    if (j === 1) {
      const formatter = 'YYYY-MM-DD';

      const time = moment(new Date(this.dataarray[i].valeur1).toLocaleString()).format(formatter);
      this.dataarray[i].valeur1 = time;

    } else if (j === 2) {
      const formatter = 'YYYY-MM-DD';

      const time = moment(new Date(this.dataarray[i].valeur2).toLocaleString()).format(formatter);
      this.dataarray[i].valeur2 = time;
    }

  }

}
